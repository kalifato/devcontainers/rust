# Rust in container

This repository contains a PoC of running a simple rust application (guess my name game) in a container.

The code used in this PoC is extracted from the book _The Rust Programming Language_.

## Development

A .devcontainer folder it's provided to simplify user experience.

## Build
The dockerfile provided is a multistage Dockerfile so builder and runtime container are different.

To build the container simply launch this command in a terminal:

```
docker build -t guess-my-number:scratch -f Dockerfile .
```

## Run
To run the container, an interactive terminal must be set within the docker run:

```
docker run -it guess-my-number:scratch
```

## Links 

 - [How to create small Docker images for Rust
](https://kerkour.com/rust-small-docker-image/)
 - [The Rust Programming Language](https://doc.rust-lang.org/book/ch02-00-guessing-game-tutorial.html).
