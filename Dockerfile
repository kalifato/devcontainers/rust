####################################################################################################
## Builder
####################################################################################################
FROM rust:latest AS builder

RUN rustup target add x86_64-unknown-linux-musl
RUN apt update && apt install -y musl-tools musl-dev
RUN update-ca-certificates

# Create appuser
ENV USER=user
ENV UID=10001

RUN adduser \
    --disabled-password \
    --gecos "" \
    --home "/nonexistent" \
    --shell "/sbin/nologin" \
    --no-create-home \
    --uid "${UID}" \
    "${USER}"


WORKDIR /opt/sources

COPY ./ .

RUN cargo build --target x86_64-unknown-linux-musl --release
RUN strip -s /opt/sources/target/x86_64-unknown-linux-musl/release/guess-my-number

####################################################################################################
## Final image
####################################################################################################
FROM scratch

# Import from builder.
COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /etc/group /etc/group

WORKDIR /opt/binaries

# Copy our build
COPY --from=builder /opt/sources/target/x86_64-unknown-linux-musl/release/guess-my-number ./

# Use an unprivileged user.
USER user:user

CMD ["/opt/binaries/guess-my-number"]
